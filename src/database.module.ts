import { Module, Global, DynamicModule } from '@nestjs/common'

import { TypeOrmModule } from '@nestjs/typeorm'


import { EnvModule } from './env.module'
import { EnvService } from './env.service';

function DatabaseOrmModule (): DynamicModule {
  const config = new EnvService().read()
  return TypeOrmModule.forRoot({
    type: config.AUTH_DB_TYPE,
    host: config.AUTH_DB_HOST,
    port: config.AUTH_DB_PORT,
    username: config.AUTH_DB_USER,
    password: config.AUTH_DB_PASSWORD,
    database: config.AUTH_DB_NAME,
    entities: ['dist/**/*.entity.{ts,js}'],
    synchronize:config.AUTH_DB_SYNCHRONIZE
  })
}

@Global()
@Module({
  imports: [
    EnvModule,
    DatabaseOrmModule()
  ]
})
export class DatabaseModule { }