import { INestMicroservice, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { join } from 'path';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from './auth/filter/http-exception.filter';
import { protobufPackage } from './auth/auth.pb';
import { EnvService } from './env.service';

async function bootstrap() {
  const config = new EnvService().read()
  const url = `${config.AUTH_HOST}:${config.AUTH_PORT}`;
  const app: INestMicroservice = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.GRPC,
    options: {
      url:url,
      package: protobufPackage,
      protoPath: join('node_modules/grpc-proto/proto/auth.proto'),
    },
  });

  app.useGlobalFilters(new HttpExceptionFilter());
  app.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }));

  await app.listen();

  console.log(`Application running at ${url}`)
}

bootstrap();
